const { merge } = require('webpack-merge')
const commonConfiguration = require('./webpack.common.js')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

const tour_slug = process.env.TOUR_SLUG;
const version = process.env.VERSION;
const language = process.env.LANG;  // Note: LANG is an existing env variable, but it should work for this purpose.

module.exports = merge(
    commonConfiguration,
    {
        mode: 'production',
        output: {
            publicPath: `/${tour_slug}${version}-${language}/`
        },
        plugins:
        [
            new CleanWebpackPlugin()
        ]
    }
)

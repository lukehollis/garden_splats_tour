import DATA from './ramessesi.json';
import TOUR_DATA from './tour_ramessesi.json';
import UI from './ui.json';


DATA.TOUR_DATA = TOUR_DATA;
DATA.UI = UI;


export default DATA;

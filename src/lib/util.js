import * as THREE from 'three'


import DATA from '../../static/locales';
import Store from '../Store';


export const calculateCameraPosition = (target, distance, azimuth, polar) => {
  const newPosition = new THREE.Vector3();
  
  newPosition.x = target.x + distance * Math.sin(polar) * Math.cos(azimuth);
  newPosition.y = target.y + distance * Math.cos(polar);
  newPosition.z = target.z + distance * Math.sin(polar) * Math.sin(azimuth);
  
  return newPosition;
}


export const areVector3Equal = (vector1, vector2) => {
  return (
    vector1.x === vector2.x && vector1.y === vector2.y && vector1.z === vector2.z
  );
}

export const makeImageSrc = ({ image, files }) => {
  let img_src = null;

  if (typeof image !== "undefined" && image.length) {
    if (image.indexOf(".gif") >= 0) {
      img_src = `https://static.mused.org/${image}`;
    } else {
      img_src = `https://iiif.mused.org/${image}/square/600,/0/default.jpg`;
    }
  } else if (files && files.length) {
    if (files[0].mime_type.indexOf(".gif") >= 0) {
      img_src = `https://static.mused.org/${files[0].filename}`;
    } else {
      img_src = `https://iiif.mused.org/${files[0].filename}/square/600,/0/default.jpg`;
    }
  }

  return img_src;
}

export const makeVideoSrc = ({ video, files }) => {
  let video_src = null;

  if (typeof video !== "undefined" && video.length) {
    video_src = `https://static.mused.org/${video}`;
  } else if (files && files.length) {
    video_src = `https://static.mused.org/${files[0].filename}`;
  }

  return video_src;
}


export const makeMapSrc = (map) => {
  if (map.indexOf("https") === 0) {
    return map;
  } else {
    return `https://www.google.com/maps/embed/v1/place?key=AIzaSyDHZUm3dFeiYQ773mXMSSkvH9BZ9B00M3s&q=${map}&maptype=satellite&zoom=8`;
  }
}

// Function to calculate the Euclidean distance between two points
export const calculateDistance = (pointA, pointB) => {
  const dx = pointA.x - pointB.x;
  const dy = pointA.y - pointB.y;
  const dz = pointA.z - pointB.z;
  return Math.sqrt(dx * dx + dy * dy + dz * dz);
};


export const makeTextureTemplateUrls = (textureUrl) => {
  // resolutions
  const resolutions = ["128,", "512,", "1024,", "2048,",];

  const urls = resolutions.map((resolution) => textureUrl.replace('{resolution}', resolution));

  // IIIF messes up lighting and image quality sometimes on resizing -- need to examine in the future, but for now, just load the static version also
  urls.push(textureUrl.replace("https://iiif.mused.org", "https://static.mused.org").replace("/full/{resolution}/0/default.jpg", ""))

  return urls;
}


export const getInitialOrbitTarget = () => {
  const sceneSettings = DATA[window.LANG].sceneSettings;
  const initialNavPoint = DATA[window.LANG].navPoints[DATA[window.LANG].initialNavPoint];

  // Create a matrix to represent the scene's transformations
  const sceneMatrix = new THREE.Matrix4();

  // Apply the scene's position offset
  const scenePosition = new THREE.Vector3(
    sceneSettings.offsetPosition.x,
    sceneSettings.offsetPosition.y,
    sceneSettings.offsetPosition.z
  );
  sceneMatrix.setPosition(scenePosition);

  // Apply the scene's rotation offset
  const sceneRotation = new THREE.Euler(
    THREE.MathUtils.degToRad(sceneSettings.offsetRotation.x),
    THREE.MathUtils.degToRad(sceneSettings.offsetRotation.y),
    THREE.MathUtils.degToRad(sceneSettings.offsetRotation.z),
    'XYZ'
  );
  sceneMatrix.makeRotationFromEuler(sceneRotation);

  // Get the initial nav point's position
  const initialNavPointPosition = new THREE.Vector3(
    initialNavPoint.position.x,
    initialNavPoint.position.y,
    initialNavPoint.position.z
  );

  // Transform the initial nav point's position from local to world coordinates
  const worldPosition = initialNavPointPosition.applyMatrix4(sceneMatrix);

  return worldPosition;
};

export const getInitialCameraPosition = () => {
  // Get the initial orbit target position
  const orbitTarget = getInitialOrbitTarget();

  // Get the azimuth and polar angles in radians
  const azimuth = THREE.MathUtils.degToRad(DATA[window.LANG].initialRotation.azimuth);
  const polar = THREE.MathUtils.degToRad(DATA[window.LANG].initialRotation.polar);

  // Calculate the distance from the target you want the camera to be
  const radius = 0.1; 

  // Calculate the camera position based on spherical coordinates
  const x = orbitTarget.x + radius * Math.sin(polar) * Math.cos(azimuth);
  const y = orbitTarget.y + radius * Math.cos(polar);
  const z = orbitTarget.z + radius * Math.sin(polar) * Math.sin(azimuth);

  // Return the initial camera position
  return new THREE.Vector3(x, y, z);
};



export const useCachedTexture = (uuidSlug, faceI, maxRes = "full", onFullResLoaded = null) => {
  const { textures } = Store.getState();
  const url1k = getTextureUrl(uuidSlug, faceI, "1024");
  const url4k = getTextureUrl(uuidSlug, faceI, "full");

  const textureLoader = new THREE.TextureLoader();

  // Check if the 1k texture is already in the cache
  if (!textures[url1k]) {
    // Load the 1k texture if not in the cache
    textures[url1k] = textureLoader.load(url1k);
    textures[url1k].minFilter = THREE.LinearMipMapLinearFilter;
    textures[url1k].magFilter = THREE.LinearFilter;
    textures[url1k].anisotropy = 16;
    textures[url1k].colorSpace = THREE.SRGBColorSpace;
    textures[url1k].needsUpdate = true;
  }

  // If maxRes is "full" and the 4k texture is not already in the cache, load it
  if (maxRes === "full" && !textures[url4k]) {
    textureLoader.load(url4k, (texture) => {
      texture.minFilter = THREE.LinearMipMapLinearFilter;
      texture.magFilter = THREE.LinearFilter;
      texture.anisotropy = 16;
      texture.colorSpace = THREE.SRGBColorSpace;
      texture.needsUpdate = true;
      textures[url4k] = texture;

      // Call the callback if provided
      if (onFullResLoaded) {
        onFullResLoaded(texture);
      }
    });
  }

  // If maxRes is "1024" or the 4k texture is not yet loaded, return the 1k texture
  if (maxRes === "1024" || !textures[url4k]) {
    return textures[url1k];
  }

  // Otherwise, return the 4k texture
  return textures[url4k];
};


export const getTextureUrl = (uuidSlug, faceI, resolution="1024") => {
  let _res = resolution;

  if (_res !== "full") {
    _res += ","
  }

  // return `https://iiif.mused.org/spaceshare/${uuidSlug}_face${faceI}_4k.jpg/full/${_res}/0/default.jpg`
  // return `https://iiif.mused.org/spaceshare/${uuidSlug}_face${faceI}_2k.jpg/full/${_res}/0/default.jpg`
  // return `https://iiif.mused.org/spaceshare/${uuidSlug}v2_face${faceI}.jpg/full/${_res}/0/default.jpg`
  // pan-4k-0bcf8238902a47e28dc3f67955636fa4-skybox0.jpg

  return `https://iiif.mused.org/spaceshare/pan-4k-${uuidSlug}-skybox${faceI}.jpg/full/${_res}/0/default.jpg`

  // base 
  // return `https://iiif.mused.org/spaceshare/${uuidSlug}_face${faceI}.jpg/full/${_res}/0/default.jpg`
}
import * as THREE from 'three'


import DATA from '../static/locales';
import {
    calculateCameraPosition,
    makeTextureTemplateUrls,
    getInitialCameraPosition,
    getInitialOrbitTarget
} from './lib/util';




// settings for customization during local development
const DEBUG_MODE = false;
const VIEW_MODE = "FPV";
const initialNavPoint = DATA[window.LANG].navPoints[DATA[window.LANG].initialNavPoint];

// Canvas
const canvas = document.querySelector('canvas.webgl');

// camera
const camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.02, 10000);
// Set the initial camera position to the initial navigation point's position
camera.position.set(initialNavPoint.position.x, initialNavPoint.position.y, initialNavPoint.position.z);

// scene
const scene = new THREE.Scene();


/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
  canvas: canvas,
  antialias: true,
  // powerPreference: "high-performance",
  // stencil: false,
})
renderer.gammaFactor = 2.2;
renderer.outputEncoding = THREE.sRGBEncoding;
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2)); // Using 2 as the max value for DPR
renderer.toneMapping = THREE.LinearToneMapping;
renderer.toneMappingExposure = 2.0;
// setup settings necessary for god rays 
renderer.setSize(sizes.width, sizes.height);
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap;
renderer.shadowMap.autoUpdate = true;
renderer.shadowMap.needsUpdate = true;

// time
const clock = new THREE.Clock();



const initialStore = {
    app: null,
    camera,
    scene,
    canvas,
    sizes,
    renderer,
    clock,
    viewMode: VIEW_MODE,
    styleMode: "",
    cursor: { position: new THREE.Vector3(), rotation: new THREE.Quaternion() },
    cursorOpacity: 0,
    cameraPosition: getInitialCameraPosition(),
    lastMouseMove: Date.now(),
    zoomLvl: 0,
    fullScreenMode: false,
    debugMode: DEBUG_MODE,
    appLoaded: false,
    sceneInited: false,
    fov: 80,
    currentNavPoint: initialNavPoint,
    outgoingNavPoint: initialNavPoint,
    isNavigating: false,
    savedRotation: null,
    lerping: false,
    lerpValue: 0,
    minimapCamera: null,
    minimapRenderer: null,
    orbitControlsTarget: getInitialOrbitTarget(),
    sceneGraph: null,
    textures: {},
    materialCache: {},
    tourGuidedMode: true,
    tourModelActiveIdx: 0,
    tourPointActiveIdx: 0,
    tourLightMode: false,
    floorMarkers: [],
};


// Store.js
class Store {
    constructor() {
        this.store = initialStore;
        this.listeners = [];
    }

    getState() {
        return this.store;
    }

    setState(newStore) {
        this.store = { ...this.store, ...newStore };
        this.notifyAll();
    }

    listen(listener) {
        this.listeners.push(listener);
    }

    notifyAll() {
        this.listeners.forEach(listener => listener(this.store));
    }
}

const store = new Store();

export default store;
import './main.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'

import {
  EffectComposer,
  ClearPass,
  RenderPass,
  ShaderPass,
  BloomEffect,
  BlendFunction,
  SMAAEffect,
  KernelSize,
  Pass,
  EffectPass,
} from 'postprocessing';

import Store from './Store';
import EventManager from './EventManager';
import App from './components/App'





// Store
// Create App
Store.setState({ app: new App() });

// events
const eventManager = new EventManager();

// turn off by default while developing
// eventManager.handleGuideToggle();

// get store state for functions
const store = Store.getState();
const { app, camera, renderer, bloomLayer, sizes, scene, canvas, BLOOM_SCENE } = store;

// setup the god rays pass 
app.post.makeGodRaysPass();

const materials = {};



window.addEventListener('resize', () => {
  // Update sizes
  sizes.width = window.innerWidth
  sizes.height = window.innerHeight

  // Update camera
  camera.aspect = sizes.width / sizes.height // Using the camera from the App class
  camera.updateProjectionMatrix()

  // Update renderer
  renderer.setSize(sizes.width, sizes.height)
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

  if (app.post.bloomComposer) {
    app.post.bloomComposer.setSize(sizes.width, sizes.height);
  }
  app.post.composer.setSize(sizes.width, sizes.height); 
})


renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

// Animation
const animate = function() {
  requestAnimationFrame(animate);
  store.app.update(); // Call update method from App class

  // Render the original scene
  store.app.post.composer.render();
};


// preload textures
store.app.preload();

// call animate function
animate();
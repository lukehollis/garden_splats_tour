import * as THREE from 'three';


import Store from './Store';
import DATA from '../static/locales';
import { areVector3Equal } from './lib/util';



export default class CameraHandlers {

  constructor() {
  }

  handleNavigation(navPoint, options={ 
    viewMode: "FPV",
    orbitTarget: null, 
    distance: 10,
    position: null,
    rotation: null,
    noDollhouse: false,
  }) {

    const { 
      currentNavPoint, outgoingNavPoint, app, 
      isNavigating, viewMode, orbitControlsTarget,
      camera, debugMode, tourLightMode,
    } = Store.getState();

    if (debugMode) {
      console.log("Navigating:", navPoint, options);
    }

    // dont need to navigate if at current nav point
    if (
      (viewMode === "FPV" && options.viewMode === "FPV" && navPoint.uuid === currentNavPoint.uuid && !options.position && !options.rotation)
      ) {

      return;
    }

    // dont navigate if currently navigating between points 
    if (isNavigating) return;

    let newOrbitTarget;

    // if there is a custom orbit target set, navigate to that target (i.e. model or feature)
    if (options.orbitTarget) {
      // set custom target
      newOrbitTarget = options.orbitTarget;

    } else {
      // navigate to nav point target
      newOrbitTarget = new THREE.Vector3(navPoint.position.x, navPoint.position.y, navPoint.position.z);
    }

    // as needed set new navpoint target 
    Store.setState({ 
      orbitControlsTarget: newOrbitTarget, 
    });

    // as needed update current nav point
    if (navPoint.uuid !== currentNavPoint.uuid) {
      Store.setState({ 
        outgoingNavPoint: currentNavPoint,
        currentNavPoint: navPoint,
      });
    }

    // handle navigation for the navpoint 
    let lerpTarget = navPoint.position;
    let lookTarget = new THREE.Vector3(navPoint.position);
    let distance = 0.1;
    let rotation = null;

    // calculate distance option
    if (options.distance) {
      distance = options.distance;
    }

    // calculate position option
    if (options.position) {
      lerpTarget = options.position;
    }

    if (options.rotation) {
      const azimuth = options.rotation.azimuth;
      const polar = options.rotation.polar;

      rotation = {
        azimuth,
        polar,
      };
    }

    if (options.viewMode !== viewMode) {
      // toggle the view mode but don't lerp b/c will set manually in next step
      this.setViewMode(options.viewMode, { doLerp: false, noDollhouse: options.noDollhouse });
    }

    // lerp to new point
    app.rig.setLerpTarget(lerpTarget, rotation); 

    // Update the texture in the EnvCube
    if (viewMode === "FPV" && navPoint.uuid !== currentNavPoint.uuid) {
      app.envCube.crossfade();
    }
    
  }

  toggleViewMode() {
    const { viewMode } = Store.getState();

    // Toggle between the modes
    const newMode = viewMode === "FPV" ? "ORBIT" : "FPV";
    this.setViewMode(newMode);
  }

  setViewMode(newMode="FPV", options={doLerp: true, noDollhouse: false }) {
    const { viewMode, app, camera, orbitControlsTarget } = Store.getState();
    
    // Update the UI
    const cubeSVG = document.getElementById("view-mode-fpv-icon");
    const walkSVG = document.getElementById("view-mode-orbit-icon");
    if (newMode === "FPV") {
      cubeSVG.classList.remove("hidden");
      walkSVG.classList.add("hidden");
    } else {
      cubeSVG.classList.add("hidden");
      walkSVG.classList.remove("hidden");
    }

    let newCameraPosition = null;

    // Set new lerp target on the rig
    if (newMode === "FPV") {
      newCameraPosition = orbitControlsTarget.clone().add(camera.getWorldDirection(new THREE.Vector3()).multiplyScalar(-0.1));

    } else {
      newCameraPosition = orbitControlsTarget.clone().add(camera.getWorldDirection(new THREE.Vector3()).multiplyScalar(-10));
    }

    // Update the Store with the new mode
    Store.setState({
      viewMode: newMode, 
    });

    // lerp to new camera position 
    if (options.doLerp) {
      app.rig.setLerpTarget(newCameraPosition);
      Store.setState({ 
        cameraPosition: newCameraPosition, 
      });
    }

    // update the orbit controls for new View mode
    app.rig.updateOrbitControlsViewMode();

    // handle updating settings for viewing in different view mode
    if (options.noDollhouse) {
      app.dollhouse.hide();
    } else {
      app.dollhouse.handleToggleViewMode();
    }

    app.envCube.handleToggleViewMode();
    app.navPoints.handleToggleViewMode();

    if (app.rig.flashlight) {
      // toggle flashlight
      if (newMode === "FPV") {
        // turn on flashlight
        app.rig.flashlight.show();
      } else {
        // turn on flashlight
        app.rig.flashlight.hide();
      }

    }
  }


  handleZoom(zoomAmount) {
    let { camera, zoomLvl, fov } = Store.getState();

    zoomLvl += zoomAmount;

    if (zoomLvl > 90) zoomLvl = 90;
    if (zoomLvl < 20) zoomLvl = 20;

    fov = 110 - zoomLvl;

    camera.fov = fov;
    camera.updateProjectionMatrix();

    // Update the store with the new zoom level and fov
    Store.setState({ zoomLvl, fov });
  }
 }

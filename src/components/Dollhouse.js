import * as THREE from 'three';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader';

import Store from '../Store';
import DATA from '../../static/locales';


export default class Dollhouse {
  constructor() {
    const { viewMode, debugMode, scene } = Store.getState();
    this.gltf = null;

    // Environment orientation
    const position = new THREE.Vector3(DATA[window.LANG].sceneSettings.model.offsetPosition.x, DATA[window.LANG].sceneSettings.model.offsetPosition.y, DATA[window.LANG].sceneSettings.model.offsetPosition.z);
    const rotation = new THREE.Euler(THREE.MathUtils.degToRad(DATA[window.LANG].sceneSettings.model.offsetRotation.x), THREE.MathUtils.degToRad(DATA[window.LANG].sceneSettings.model.offsetRotation.y), THREE.MathUtils.degToRad(DATA[window.LANG].sceneSettings.model.offsetRotation.z));
    const scale = DATA[window.LANG].sceneSettings.model.scale;

    // Group to contain the Dollhouse components
    this.group = new THREE.Group();
    this.group.position.copy(position);
    this.group.rotation.copy(rotation);``
    this.group.scale.set(scale, scale, scale);
    scene.add(this.group);
    this.gltf = null;

    // Load the model for the dollhouse
    const loader = new GLTFLoader();
    try {
      loader.load(DATA[window.LANG].dollhouse, gltf => this.onLoad(gltf), undefined, function (error) {
        console.error('An error occurred while loading the model:', error);
      });
    } catch (e) {
      console.error('Caught error:', e);
    }

    // FPV occlusions
    this.occluderMat = new THREE.MeshBasicMaterial({
      colorWrite: false, // Don't write to color buffer
      transparent: true,
    });
  }

  onLoad(gltf) {

    const { scene } = Store.getState();
    gltf.scene.renderOrder = 1;

    // save gltf to class for future use
    this.gltf = gltf;

    // do the common logic of rendering the mesh in different states with different transparency
    this._updateTransparency();

    // add the gltf to the scene
    scene.add(gltf.scene);
  }

  handleToggleDebugMode() {
    // do the common logic of rendering the mesh in different states with different transparency
    this._updateTransparency();
  }

  handleToggleViewMode() {
    // do the common logic of rendering the mesh in different states with different transparency
    this._updateTransparency();
  }

  // Show the dollhouse
  show() {
    const { scene } = Store.getState();
    if (!this.gltf || !this.gltf.scene) {
      console.warn('Dollhouse not loaded yet.');
      return;
    }
    this.gltf.scene.visible = true;

    // Remove existing occluders
    // const existingOccluders = [];
    // scene.traverse((child) => {
    //   if (child.name === 'occluder') {
    //     existingOccluders.push(child);
    //   }
    // });
    // existingOccluders.forEach(occluder => {occluder.visible = false} );

    this.gltf.scene.traverse((child) => {
      this.gltf.scene.traverse((child) => {
        if (child.isMesh) {
          child.visible = true;
        }
      });
    });
  }

  // Hide the dollhouse
  hide(options={ hideOccluders: false }) {
    const { scene } = Store.getState();

    if (!this.gltf || !this.gltf.scene) {
      console.warn('Dollhouse not loaded yet.');
      return;
    }
    this.gltf.scene.visible = false;

    this.gltf.scene.traverse((child) => {
      this.gltf.scene.traverse((child) => {
        if (child.isMesh) {
          child.visible = false;
        }
      });
    });

    this.hideOccluders();
  }

  hideOccluders() {
    const { scene } = Store.getState();
    const existingOccluders = [];
    scene.traverse((child) => {
      if (child.name === 'occluder') {
        existingOccluders.push(child);
      }
    });
    existingOccluders.forEach(occluder => {occluder.visible = false } );
  }

  showOccluders() {
    const { scene } = Store.getState();
    const existingOccluders = [];
    scene.traverse((child) => {
      if (child.name === 'occluder') {
        existingOccluders.push(child);
      }
    });
    existingOccluders.forEach(occluder => {occluder.visible = true} );
  }

  // Toggle the dollhouse visibility
  toggleVisibility() {
    if (!this.gltf || !this.gltf.scene) {
      console.warn('Dollhouse not loaded yet.');
      return;
    }
    this.gltf.scene.visible = !this.gltf.scene.visible;
  }


  _updateTransparency() {
    const { 
      debugMode, viewMode, scene,
      tourLightMode
    } = Store.getState();
    const transparent = (viewMode === "FPV") && !debugMode;
    const renderOrder = viewMode === "ORBIT" || debugMode ? 2 : 1;

    if (this.gltf && this.gltf.scene) {

      // Remove existing occluders
      const existingOccluders = [];
      scene.traverse((child) => {
        if (child.name === 'occluder') {
          existingOccluders.push(child);
        }
      });
      existingOccluders.forEach(occluder => scene.remove(occluder));

      // set transparencies
      this.gltf.scene.traverse((child) => {
        if (child.isMesh) {
          child.material.transparent = true;
          child.material.needsUpdate = true;
          child.material.opacity = transparent ? 0 : 1;
          child.material.wireframe = debugMode;
          child.material.side = THREE.FrontSide;
          child.renderOrder = renderOrder;

          if (viewMode === "ORBIT") {
            child.material.depthWrite = true;
            child.material.depthTest = true;
          } else {
            child.material.depthWrite = false;
            child.material.depthTest = false;
          }
        }
      });

      // handle occluders
      if (viewMode === "FPV" && !debugMode) {
        this.gltf.scene.traverse((child) => {
          if (child.isMesh) {
            // FPV occlusions
            const occluder = new THREE.Mesh(child.geometry, this.occluderMat);
            occluder.name = 'occluder'; // Give the occluder a name
            occluder.position.set(child.position.x, child.position.y, child.position.z);
            occluder.rotation.set(child.rotation.x, child.rotation.y, child.rotation.z);
            occluder.renderOrder = renderOrder + 1;
            scene.add(occluder);
          }

        });
      }


    }


    // hack for tour mode for model, will come back to for refactor
    if (tourLightMode) {
      this.hide();      
    } else {
      this.show();
    }
  }

}


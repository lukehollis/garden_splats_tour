import * as THREE from 'three';



import Store from '../Store';



export default class Skybox {
  constructor() {
    // global store
    const { 
      scene
    } = Store.getState();

    scene.background = new THREE.Color(0xe2dac9);
    // scene.background = new THREE.TextureLoader().load("https://iiif.mused.org/dig-notebook-paper-bg-v2.jpg/full/3000,/0/default.jpg");

    // fog

   // if needed for fog over skybox // // Add a background box for fog over skybox
    // const boxGeo = new THREE.SphereGeometry(2100, 2100, 2100);
    // // Create material with inverted textures
    // const boxMat = new THREE.MeshBasicMaterial({
    //     color: 0x000000, 
    //     side: THREE.BackSide  // This makes the material only visible from the inside of the box
    // });
    // // Create the box mesh
    // const box = new THREE.Mesh(boxGeo, boxMat);
    // // Position the box at the center of the scene
    // box.position.set(0, 0, 0);
    // scene.add(box);


    // const intensity = 150;
    // const distance = 1200;
    // const angle = 0.1;
    // const penumbra = 0.6;
    // const decay = 1;
    // const light = new THREE.SpotLight( color, intensity, distance, angle, penumbra, decay);
    // light.position.set( 0, 1.5, 0 );
    // light.target.position.set( - 5, 0, 0 );
    // scene.add( light );
    // scene.add( light.target );

    // const spotLightHelper = new THREE.SpotLightHelper( light );
    // scene.add( spotLightHelper );

    // this.turnOnFog();
  }

  turnOnFog() {
    // global store
    const { 
      scene
    } = Store.getState();
    const color = 0xeee6d7;  
    const near = 2;
    const far = 1800;

    // scene.fog = new THREE.FogExp2(color, 0.001);
    scene.fog = new THREE.Fog(color, near, far);
  }

  turnOffFog() {
    // global store
    const { 
      scene
    } = Store.getState();
    scene.fog = null;
  }


}

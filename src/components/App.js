import * as THREE from 'three';

// global state
import Store from '../Store';
import DATA from '../../static/locales';

// components
import Rig from './Rig';
import EnvCube from './EnvCube';
import Dust from './Dust';
import NavigationPoints from './NavigationPoints';
import Dollhouse from './Dollhouse';
import Cursor from './Cursor';
import DebugInfo from './DebugInfo';
import SceneGraph from './SceneGraph';
import TourUI from './tour/TourUI';
import Photograph from './Photograph';
import CutsceneBackground from './CutsceneBackground';
import LoadingScreen from './loading/LoadingScreen';
import AudioManager from './AudioManager';
import Postprocessing from './Postprocessing'

// event handlers
import PointerHandlers from '../PointerHandlers';
import CameraHandlers from '../CameraHandlers';

// lib
import { useCachedTexture, } from '../lib/util';


function hasPass(composer, passType) {
  for (let i = 0; i < composer.passes.length; i++) {
    if (composer.passes[i] instanceof passType) {
      return true;
    }
  }
  return false;
}


export default class App {
  constructor() {
    // store
    const { scene } = Store.getState();

    // Loading screen
    this.loadingScreen = new LoadingScreen();

    // Camera
    this.rig = new Rig();

    // Create ambient light
    const ambientLight = new THREE.AmbientLight(0xeee6d7, 10);
    scene.add(ambientLight);
    this.ambientLight = ambientLight;
    this.targetIntensity = 1;
    this.currentIntensity = 1;
    this.lerpAlpha = 1;

    // The Cube with the Environment texture 
    this.envCube = new EnvCube();

    // The dollhouse 
    this.dollhouse = new Dollhouse();
    this.cutsceneBackground = new CutsceneBackground();
    this.sceneGraph = new SceneGraph();

    // The Cube with the Environment texture 
    this.navPoints = new NavigationPoints();

    // The cursor 
    this.cursor = new Cursor();

    // Handle Camera and Pointer
    this.cameraHandlers = new CameraHandlers();
    this.pointerHandlers = new PointerHandlers();

    // guided tour
    this.tourUI = new TourUI();

    // debugging
    this.debugInfo = new DebugInfo();

    // audio
    this.audioManager = new AudioManager();

    // fx
    // this.dust = new Dust();

    // post
    // must be after lights are added
    this.post = new Postprocessing();
  } 

  preload() {
    // Preload initial texture and then initialize rest of the App
    // store
    const { currentNavPoint } = Store.getState();
    this._loadNavPointMaterials(currentNavPoint);

    // Load remaining textures in the background
    const remainingNavPoints = DATA[window.LANG].navPoints.filter((np) => np.uuid !== currentNavPoint.uuid);
    for (const navPoint of remainingNavPoints) {
      // this._loadNavPointMaterials(navPoint);
    }
  }

  _loadNavPointMaterials(navPoint) {
    const uuidSlug = navPoint.uuid.replace(/-/g, '');
    const { materialCache } = Store.getState();

    materialCache[uuidSlug] = {};
    Array.from({ length: 6 }).forEach((_, faceI) => {
      materialCache[uuidSlug][faceI] = {};
      // load the 1024 version
      const texture = useCachedTexture(uuidSlug, faceI, "1024");
      const material = new THREE.MeshBasicMaterial({
        map: texture,
        transparent: true,
      });
      materialCache[uuidSlug][faceI]["1024"] = material;

      // load the full res version
      const textureFull = useCachedTexture(uuidSlug, faceI, "full");
      const materialFull = new THREE.MeshBasicMaterial({
        map: texture,
        transparent: true,
      });
      materialCache[uuidSlug][faceI]["full"] = materialFull;
    });
  }

  setLightIntensity(intensity) {
    this.targetIntensity = intensity;
    this.lerpAlpha = 0;   
  }

  // Call this function in your animation loop or in a setInterval
  updateLights() {
    if (this.lerpAlpha < 1) {
      this.lerpAlpha += 0.01; // Control the speed by changing this value
    }

    // Linearly interpolate between the current and target intensity
    this.currentIntensity = THREE.MathUtils.lerp(this.currentIntensity, this.targetIntensity, this.lerpAlpha);

    // Update the intensity of the ambient light
    this.ambientLight.intensity = this.currentIntensity;
  }

  update() {
    // manage fading in and out the light
    this.updateLights();

    this.rig.update();
    this.envCube.update();
    this.cursor.update();

    this.debugInfo.update();

    // conditional  
    if (this.photograph) {
      this.photograph.update();
    }
    if (this.cutsceneBackground) {
      this.cutsceneBackground.update();
    }

    if (this.loadingScreen) {
      this.loadingScreen.render();
    }

    if (this.dust) {
      this.dust.update();
    }
  }

}

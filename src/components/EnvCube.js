import * as THREE from 'three';

import AnnotationGraph from './AnnotationGraph';

import { useCachedTexture, } from '../lib/util';
import Store from '../Store';



class EnvCube {
  constructor(navPoint, options={ isOutgoing: false }) {
    const store = Store.getState();
    const scene = store.scene;

    this.navPoint = navPoint;
    this.options = options;
    this.raycaster = new THREE.Raycaster();
    this.group = new THREE.Group();
    this.materials = [];
    this.fullResolutionFaces = new Set(); 

    // build the face planes
    this.createFaces();

    const rotation = navPoint.rotation;
    this.group.rotation.set(rotation.x, rotation.y, rotation.z);

    scene.add(this.group);
  }

  getRotation(faceI) {
    switch (faceI) {
      case 2: return [0, Math.PI / 2, 0];
      case 4: return [0, -Math.PI / 2, 0];
      case 0: return [Math.PI / 2, 0, Math.PI];
      case 5: return [-Math.PI / 2, 0, Math.PI];
      case 1: return [0, Math.PI, 0];
      case 3: return [0, 0, 0];
      default: return [0, 0, 0];
    }
  }

  getPosition(faceI) {
    // faceI = 2 -> Left
    // faceI = 4 -> Right
    // faceI = 0 -> Top
    // faceI = 5 -> Bottom
    // faceI = 1 -> Front
    // faceI = 3 -> Back
    switch (faceI) {
      case 2: return [-100, 0, 0];
      case 4: return [100, 0, 0];
      case 0: return [0, 100, 0];
      case 5: return [0, -100, 0];
      case 1: return [0, 0, 100];
      case 3: return [0, 0, -100];
      default: return [0, 0, 0];
    }
  }

  createFaces() {
    const size = 200;
    this.uuidSlug = this.navPoint.uuid.replace(/-/g, '');

    Array.from({ length: 6 }).forEach((_, faceI) => {
      const texture = useCachedTexture(this.uuidSlug, faceI, "1024");

      // const material = new THREE.MeshPhongMaterial({
      const material = new THREE.MeshBasicMaterial({
        map: texture,
        transparent: true,
        opacity: this.options.isOutgoing ? 0 : 1,
      });
      this.materials.push(material);
    });

    // Create the faces with the materials
    this.materials.forEach((material, faceI) => {
      this.createFace(size, this.getRotation(faceI), this.getPosition(faceI), faceI);
    });
  }

  createFace(size, rotation, position, materialIndex) {
    const geometry = new THREE.PlaneGeometry(size, size);
    const mesh = new THREE.Mesh(geometry, this.materials[materialIndex]);
    mesh.rotation.set(...rotation);
    mesh.position.set(...position);
    this.group.add(mesh);
  }

  updateFaces() {
    this.uuidSlug = this.navPoint.uuid.replace(/-/g, '');

    for (let i = 0; i<this.materials.length; i++) {
      // only use the 1024 at the start 
      const texture = useCachedTexture(this.uuidSlug, i, "1024");
      this.materials[i].map = texture; 
      // this.materials[i].needsUpdate = true;
      this.fullResolutionFaces = new Set(); 
    }
  }

  getVisibleFaces(camera) {
    const visibleFaces = [];

    this.group.children.forEach((child, i) => {
      if (this.fullResolutionFaces.has(i)) return; // Skip if already at full resolution

      this.raycaster.setFromCamera(camera.position, camera);
      const intersects = this.raycaster.intersectObject(child);

      if (intersects.length > 0) {
        visibleFaces.push(i);
      }
    });

    return visibleFaces;
  }


  updateFacesFullRes(visibleFaces) {
    visibleFaces.forEach(i => {
      // Skip if already at full resolution
      if (this.fullResolutionFaces.has(i)) return;

      // Load the full-resolution 4k texture
      const texture = useCachedTexture(this.uuidSlug, i, "full");
      this.materials[i].map = texture;
      // this.materials[i].needsUpdate = true;
      this.fullResolutionFaces.add(i); // Mark this face as loaded at full resolution
    });
  }

  updateVisibleFaces() {
    const { camera } = Store.getState();
    const visibleFaces = this.getVisibleFaces(camera);
    this.updateFacesFullRes(visibleFaces);
  }
}

export default class EnvCubeManager {
  constructor() {
    const { currentNavPoint, outgoingNavPoint }  = Store.getState();

    this.envCube = new EnvCube(currentNavPoint);
    this.envCubeOutgoing = new EnvCube(outgoingNavPoint, { isOutgoing: true });
    this.annotationGraph = new AnnotationGraph(); 

    // set the cube render order based on app state
    this._setCubeRenderOrder();
  }

  crossfade() {
    const store = Store.getState();
    const { currentNavPoint, outgoingNavPoint } = store;

    // set navigating semaphore
    Store.setState({ isNavigating: true });

    // update faces on outgoing cube 
    this.envCubeOutgoing.navPoint = outgoingNavPoint; 
    this.envCubeOutgoing.updateFaces();

    // make outgoing cube cover main cube
    this.envCubeOutgoing.materials.forEach((material) => (material.opacity = 1));

    // update faces on main cube
    this.envCube.navPoint = currentNavPoint;
    this.envCube.updateFaces();

    // fade out outgoing cube
    let progress = 0;
    let duration = 600;
    const interval = setInterval(() => {
      progress += 20;
      const factor = progress / duration;

      this.envCubeOutgoing.materials.forEach((material) => (material.opacity = 1 - factor));

      if (progress >= duration) {
        Store.setState({ isNavigating: false });
        clearInterval(interval);
      }
    }, 20);
  }

  fadeOut(callback=null) {
    const store = Store.getState();

    // fade out outgoing cube
    let progress = 0;
    let duration = 200;
    const interval = setInterval(() => {
      progress += 20;
      const factor = progress / duration;

      this.envCube.materials.forEach((material) => (material.opacity = 1 - factor));

      if (progress >= duration) {
        clearInterval(interval);
        this.envCube.materials.forEach((material) => (material.visible = false));
        if (callback) {
          callback();
        }
      }
    }, 20);
  }

  dimScene(opacity) {
    this.envCube.materials.forEach((material) => (material.opacity = opacity));
  }

  fadeIn(callback=null) {
    const store = Store.getState();
    this.envCube.materials.forEach((material) => (material.opacity = 0));
    this.envCube.materials.forEach((material) => (material.visible = true));
    
    // fade out outgoing cube
    let progress = 0;
    let duration = 200;
    const interval = setInterval(() => {
      progress += 20;
      const factor = progress / duration;

      this.envCube.materials.forEach((material) => (material.opacity = factor));

      if (progress >= duration) {
        clearInterval(interval);
        if (callback) {
          callback();
        }
      }
    }, 20);
  }

  handleToggleDebugMode() {
    this._setCubeRenderOrder();
  }

  handleToggleViewMode() {
    const { debugMode, viewMode } = Store.getState();
    this._setCubeRenderOrder();

    if (viewMode === "FPV") {
      this.fadeIn();
    } else {
      this.fadeOut();
    }
  }

  _setCubeRenderOrder() {
    const { debugMode, viewMode } = Store.getState();
    
    // Calculate the EnvCube's render order based on the current viewMode and debugMode
    const cubeRenderOrder = (viewMode === "FPV" && !debugMode) ? 1 : 0;
    // const cubeRenderOrder = 0;

    // Apply the render order to each mesh in the EnvCube
    this.envCube.group.children.forEach(child => {
      child.renderOrder = cubeRenderOrder;
    });

    // Apply the render order to each mesh in the envCubeOutgoing
    this.envCubeOutgoing.group.children.forEach(child => {
      child.renderOrder = cubeRenderOrder;
    });

  }

  toggleMaterialType(makeUnlit=true) {
    // Iterate through the materials of the envCube
    this.envCube.materials.forEach((material, index) => {
      // Create a new material instance based on the opposite type
      let newMaterial;
      if (makeUnlit) {
        newMaterial = new THREE.MeshBasicMaterial({
          map: material.map,
          transparent: material.transparent,
          opacity: material.opacity
        });
      } else {
        newMaterial = new THREE.MeshPhongMaterial({
          map: material.map,
          transparent: material.transparent,
          opacity: material.opacity
        });
      }
      // Replace the old material with the new one
      this.envCube.materials[index] = newMaterial;
      // Update the mesh to use the new material
      this.envCube.group.children[index].material = newMaterial;
    });

    // Do the same for envCubeOutgoing
    this.envCubeOutgoing.materials.forEach((material, index) => {
      let newMaterial;
      if (makeUnlit) {
        newMaterial = new THREE.MeshBasicMaterial({
          map: material.map,
          transparent: material.transparent,
          opacity: material.opacity
        });
      } else {
        newMaterial = new THREE.MeshPhongMaterial({
          map: material.map,
          transparent: material.transparent,
          opacity: material.opacity
        });
      }
      this.envCubeOutgoing.materials[index] = newMaterial;
      this.envCubeOutgoing.group.children[index].material = newMaterial;
    });
  }


  update() {
    // figure out a better way to update to 4k faces 
    this.envCube.updateVisibleFaces();
  }
}


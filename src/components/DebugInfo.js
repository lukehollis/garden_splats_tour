import * as THREE from 'three';
import Stats from 'three/examples/jsm/libs/stats.module.js';

import Store from '../Store';
import DATA from '../../static/locales';



export default class DebugInfo {
  constructor() {
    this.container = document.getElementById('debug-container'); // The DOM element where you want to append the debug info
    this.stats = new Stats();
    this.debugMode = false; // Default value
    this.currentNavPoint = null;
  }

  setNavPoint(navPoint) {
    this.currentNavPoint = navPoint;
  }

  handleToggleDebugMode() {
    const store = Store.getState();
    const { debugMode } = store;

    if (debugMode) {
      this.showDebugInfo();
    } else {
      this.hideDebugInfo();
    }
  }
    
  showDebugInfo() {
    const { currentNavPoint } = Store.getState();

    // Append the FPS counter
    this.container.appendChild(this.stats.dom);

    // Style and show the currentNavPoint info
    if (currentNavPoint) {
      const debugText = document.createElement('span');
      debugText.class = 'fixed top-2 right-2 block text-white text-xs text-right rounded-lg p-2 hover:bg-black/10';
      debugText.innerHTML = `
        ${currentNavPoint.uuid}:
        <br>
        ${currentNavPoint.position.x.toFixed(3)}, ${currentNavPoint.position.y.toFixed(3)}, ${currentNavPoint.position.z.toFixed(3)}
        <br>
        ${currentNavPoint.rotation.x.toFixed(3)}, ${currentNavPoint.rotation.y.toFixed(3)}, ${currentNavPoint.rotation.z.toFixed(3)}
      `;
      this.container.appendChild(debugText);
    }
  }

  hideDebugInfo() {
    // Remove the FPS counter and the debug text
    while (this.container.firstChild) {
      this.container.removeChild(this.container.firstChild);
    }
  }

  update() {
    const store = Store.getState();
    const { debugMode } = store;
    if (debugMode) {
      this.stats.update();
    }
  }
}


import * as THREE from 'three';

import Store from '../Store';


export default class FloorMarker {
  constructor({ uuid, position, floorPosition, rotation, image, }) {
    const { scene, floorMarkers } = Store.getState();

    this.uuid = uuid;
    this.position = position;
    this.floorPosition = floorPosition;
    this.rotation = rotation;
    this.image = image;

    this.group = new THREE.Group();
    this.group.position.copy(floorPosition);

    // The marker plane
    const planeGeometry = new THREE.PlaneGeometry(0.6, 0.6, 1);
    const planeMaterial = new THREE.MeshStandardMaterial({
      color: "#ffffff",
      side: THREE.DoubleSide,
      transparent: true,
      opacity: 0,
      depthWrite: false
    });
    const planeMesh = new THREE.Mesh(planeGeometry, planeMaterial);
    planeMesh.floorMarker = this;  // Back reference
    this.group.add(planeMesh);

    // The ring around the marker
    const ringGeometry = new THREE.RingGeometry(0.2, 0.25, 32);
    const ringMaterial = new THREE.MeshStandardMaterial({
      color: "#ffffff",
      side: THREE.DoubleSide,
      transparent: true,
      opacity: 0.1,
      depthWrite: true,
      depthTest: true
    });
    const ringMesh = new THREE.Mesh(ringGeometry, ringMaterial);
    ringMesh.renderOrder = 4;
    this.group.add(ringMesh);

    // Adjust position and rotation
    this.group.position.copy(floorPosition);
    this.group.rotation.x = Math.PI / 2;
    scene.add(this.group);

    const newFloorMarkers = [...floorMarkers, this.group];
    Store.setState({ floorMarkers: newFloorMarkers });
  }

  onMouseEnter() {
    const { viewMode } = Store.getState();
    this.group.children.forEach(child => {
      if (child.geometry.type === 'RingGeometry') {
        if (viewMode === "ORBIT") {
          child.material.opacity = 0.7;
        } else {
          child.material.opacity = 0.3;
        }
      }
    });
  }

  onMouseLeave() {
    const { viewMode } = Store.getState();
    this.group.children.forEach(child => {
      if (child.geometry.type === 'RingGeometry') {
        if (viewMode === "ORBIT") {
          child.material.opacity = 0.6;
        } else {
          child.material.opacity = 0.1;
        }
      }
    });
  }

}
import * as THREE from 'three';





export default class AudioManager {
  constructor() {
    this.listener = new THREE.AudioListener();
    this.audioLoader = new THREE.AudioLoader();
    this.currentNarration = null;
    this.fadeOutInterval = null;
    this.isMuted = false;

    this.sounds = {
      "default": { 
        url: "https://static.mused.org/sounds/default_oud_64k.mp3", 
        audio: null,
        options: { loop: true, volume: 0.2, autoplay: false }
      },
      "sunset": { 
        url: "https://static.mused.org/sounds/netherworld_64k.mp3", 
        audio: null,
        options: { loop: true, volume: 0.1, autoplay: false }
      },
      "sunrise": { 
        url: "https://static.mused.org/sounds/sunrise_oud_64k.mp3", 
        audio: null,
        options: { loop: true, volume: 0.3, autoplay: false }
      },
      "navigate": { 
        url: "https://static.mused.org/sounds/bassdrum_64k.mp3", 
        audio: null,
        options: { loop: false, volume: 0.3, autoplay: false }
      },
      "start": { 
        url: "https://static.mused.org/sounds/toms_64k.mp3", 
        audio: null,
        options: { loop: false, volume: 0.3, autoplay: false }
      },
    };


    // Add narration tracks
    for (let i = 0; i <= 28; i++) {
      // this.sounds[`jalia_${i}`] = {
      //   url: `https://static.mused.org/sounds/jalia_v2_${i}_64k.mp3`,
      //   audio: null,
      //   options: { loop: false, volume: 1, autoplay: false }
      // };
    }

    this.preloadSounds();
  }

  loadSound(name) {
    if (!this.sounds[name]) {
      console.error(`Sound with name ${name} does not exist.`);
      return;
    }
    const sound = new THREE.Audio(this.listener);
    const { url, options } = this.sounds[name];
    this.audioLoader.load(url, (buffer) => {
      sound.setBuffer(buffer);
      sound.setLoop(options.loop);
      sound.setVolume(options.volume);
      this.sounds[name].audio = sound;

      // Auto-play if the option is set
      if (options.autoplay) {
        sound.play();
      }
    });
  }


  preloadSounds() {
    for (const name in this.sounds) {
      this.loadSound(name);
    }
  }


  // Add this method to your AudioManager class
  playSound(name) {
    const sound = this.sounds[name]?.audio;
    if (sound) {
      sound.stop();
      sound.play();
    } else {
      console.error(`Sound ${name} not found or not yet loaded`);
    }
  }

  stopSound(name) {
    const sound = this.sounds[name]?.audio;
    if (sound && sound.isPlaying) {
      sound.stop();
    }
  }

  playNarration(name) {
    // If there's a current narration, fade it out
    if (this.currentNarration) {
      this.fadeOut(this.currentNarration, 50, 0.2, this.playSound(name)); // 50ms interval, 0.01 volume decrement
    } else {
      this.playSound(name);
    }

    this.currentNarration = name;
  }

  fadeOut(name, interval = 50, decrement = 0.2, callback) {
    const sound = this.sounds[name]?.audio;
    if (!sound || !sound.isPlaying) return;

    if (this.fadeOutInterval) {
      clearInterval(this.fadeOutInterval);
    }

    this.fadeOutInterval = setInterval(() => {
      if (sound.getVolume() <= 0) {
        sound.stop();
        clearInterval(this.fadeOutInterval);

        if (callback) {
          callback();
        }
      } else {
        sound.setVolume(Math.max(sound.getVolume() - decrement, 0));
      }
    }, interval);
  }


  crossfade(fromName, toName, duration=1000) {
    const fromSound = this.sounds[fromName]?.audio;
    const toSound = this.sounds[toName]?.audio;
    const fromSoundOptions = this.sounds[fromName]?.options;
    const toSoundOptions = this.sounds[toName]?.options;

    if (!fromSound || !toSound || this.isMuted) return;

    if (!toSound.isPlaying) {
      toSound.setVolume(0);
      toSound.play();
    }

    let startTime = null;

    const step = (timestamp) => {
      if (!startTime) startTime = timestamp;
      const elapsed = timestamp - startTime;

      if (elapsed < duration) {
        const ratio = elapsed / duration;
        fromSound.setVolume(fromSoundOptions.volume - (ratio * fromSoundOptions.volume));
        toSound.setVolume(ratio * fromSoundOptions.volume);

        requestAnimationFrame(step);
      } else {
        fromSound.setVolume(0);
        toSound.setVolume(toSoundOptions.volume);
      }
    };

    requestAnimationFrame(step);
  }

  muteAll(shouldMute) {
    this.isMuted = shouldMute;
    for (const key in this.sounds) {
      const sound = this.sounds[key]?.audio;
      const options = this.sounds[key]?.options;
      if (sound) {
        if (this.isMuted) {
          sound.setVolume(0); // Silences the audio
        } else {
          sound.setVolume(options.volume); // Set back to the original volume
        }
      }
    }
  }
}

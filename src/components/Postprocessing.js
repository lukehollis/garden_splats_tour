import * as THREE from 'three'

import {
  EffectComposer,
  RenderPass,
  SelectiveBloomEffect,
  OutlineEffect,
  BlendFunction,
  SMAAEffect,
  KernelSize,
  Pass,
  EffectPass,
  ShaderPass,
} from 'postprocessing';

import { GodraysPass } from 'three-good-godrays';

import Store from '../Store';


/** 
 * Post
 */

export default class Postprocessing { 

  constructor() {
    const { camera, renderer } = Store.getState();

    this.sizes = {
      width: window.innerWidth,
      height: window.innerHeight,
    }

    this.setupPasses();
  }

  setupPasses() {
    const { scene, camera, renderer } = Store.getState();

    const renderPass = new RenderPass(scene, camera);

    // const bloom = new SelectiveBloomEffect(scene, camera, {
    //   blendFunction: BlendFunction.LINEAR_DODGE,
    //   mipmapBlur: true,
    //   luminanceThreshold: 0,
    //   luminanceSmoothing: 0.3,
    //   intensity: 1,
    //   radius: 0.7,
    // }); 

    const bloom = new SelectiveBloomEffect(scene, camera, {
      intensity: 1,
    }); 
    const outline = new OutlineEffect(scene, camera, {
      blur: true, 
    }); 
    const outlinePass = new EffectPass(camera, outline);
    const bloomPass = new EffectPass(camera, bloom);

    const composer = new EffectComposer(renderer);
    composer.addPass(renderPass);
    // composer.addPass(outlinePass);
    // composer.addPass(bloomPass);

    this.bloom = bloom; 
    this.outline = outline; 
    this.composer = composer;
  }

  // have to run this after lights are setup
  makeGodRaysPass() {
    const { app, camera, composer } = Store.getState();
    const godraysPass = new GodraysPass(app.cutsceneBackground.dirLight, camera, {
      density: 0.001,
      maxDensity: .03,
      distanceAttenuation: 8,
      color: app.cutsceneBackground.dirLight.color, 
      edgeStrength: 2,
      edgeRadius: 2,
      raymarchSteps: 60,
      enableBlur: false,
      blurVariance: 0.1,
      blurKernelSize: 1, 
    });
    godraysPass.renderToScreen = false;

    const smaaEffect = new SMAAEffect();
    const smaaPass = new EffectPass(camera, smaaEffect);
    smaaPass.renderToScreen = true;

    this.godraysPass = godraysPass;
    this.smaaPass = smaaPass;

    this.turnOnGodRays();
  }

  turnOnGodRays() {

    if (this.composer.passes.includes(this.smaaPass)) {
      this.composer.removePass(this.smaaPass);
    }

    this.composer.addPass(this.godraysPass);
    this.composer.addPass(this.smaaPass);

  }

  turnOffGodRays() {

    if (this.composer.passes.includes(this.godraysPass)) {
      this.composer.removePass(this.godraysPass);
    }
  }

}


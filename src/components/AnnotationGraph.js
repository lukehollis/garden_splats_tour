import * as THREE from 'three';
import { TransformControls } from 'three/examples/jsm/controls/TransformControls.js';
import * as dat from 'dat.gui';

import Store from '../Store';



const annotationData = {
  type: 'group',
  id: 'root',
  position: [0, 0, 0],
  children: [
    /**
     *  wall 1
     */
    {
      id: 'ramesses-wall1',
      type: 'annotation',
      navPointId: "54b1ff4a-206a-4d25-a8a2-64e5491af8cc",
      file: 'https://static.mused.org/ramesses_wall1_alt_64.png',
      position: [100, 36, -34],
      rotation: [0, 4.9, 0],
      size: [62, 122]
    },
    {
      id: 'maat-wall1',
      type: 'annotation',
      navPointId: "54b1ff4a-206a-4d25-a8a2-64e5491af8cc",
      file: 'https://static.mused.org/maat_wall1_v2_64.png',
      position: [100, 40, 18],
      rotation: [0, 4.7, 0],
      size: [46, 126]
    },
    {
      id: 'nefertem-wall1',
      type: 'annotation',
      navPointId: "54b1ff4a-206a-4d25-a8a2-64e5491af8cc",
      file: 'https://static.mused.org/nefertem_wall1_v3_64.png',
      position: [100, 36, -80],
      rotation: [0.211, -1.417, 0.206],
      size: [64, 128]
    },
    {
      id: 'ramessesi-cartouche-wall1',
      type: 'annotation',
      navPointId: "54b1ff4a-206a-4d25-a8a2-64e5491af8cc",
      file: 'https://static.mused.org/ramessesi_cartouche_wall1_alt_v3_64.png',
      position: [100, 107, -43],
      rotation: [1.2, -1.08, 1.09],
      size: [33, 30]
    },

    /**
     *  wall 2
     */
    {
      id: 'apophis-wall2',
      type: 'annotation',
      navPointId: "54b1ff4a-206a-4d25-a8a2-64e5491af8cc",
      file: 'https://static.mused.org/apophis8_64.png',
      position: [-1.3, 2.5, -100],
      rotation: [0, 0, 0],
      size: [74, 42]
    },
    {
      id: 'atum-wall2',
      type: 'annotation',
      navPointId: "54b1ff4a-206a-4d25-a8a2-64e5491af8cc",
      file: 'https://static.mused.org/atum_wall2_v3_64.png',
      position: [34, 1.5, -100],
      rotation: [0, 0, 0],
      size: [22, 42]
    },
    {
      id: 'ra-solar-boat-wall2',
      type: 'annotation',
      navPointId: "54b1ff4a-206a-4d25-a8a2-64e5491af8cc",
      file: 'https://static.mused.org/ra_solar_boat_wall2_v2_64.png',
      position: [18.5, 44, -100],
      rotation: [-0.018, 0.1705, 0],
      size: [79, 49]
    },
    {
      id: 'apophis-wall2-netherworld',
      type: 'annotation',
      navPointId: "54b1ff4a-206a-4d25-a8a2-64e5491af8cc",
      file: 'https://static.mused.org/apophis7_64.png',
      position: [-1.3, 2.5, -100],
      rotation: [0, 0, 0],
      size: [64, 32]
    },
    {
      id: 'atum-wall2-netherworld',
      type: 'annotation',
      navPointId: "54b1ff4a-206a-4d25-a8a2-64e5491af8cc",
      file: 'https://static.mused.org/atum_wall2_v2_64.png',
      position: [34, 1.5, -100],
      rotation: [0, 0, 0],
      size: [12, 32]
    },
    {
      id: 'ra-solar-boat-wall2-netherworld',
      type: 'annotation',
      navPointId: "54b1ff4a-206a-4d25-a8a2-64e5491af8cc",
      file: 'https://static.mused.org/ra_solar_boat_wall2_64.png',
      position: [17, 44, -100],
      rotation: [0, 0.149, -0.07],
      size: [68, 39]
    },
    {
      id: 'barque-of-the-earth-wall2',
      type: 'annotation',
      navPointId: "be9352fb-4ceb-4253-a78f-870971463975",
      file: 'https://static.mused.org/barque_of_earth_wall2_v2_64.png',
      position: [-15, 38, -100],
      rotation: [0, 0.15, 0],
      size: [133, 52]
    },

    /**
     * wall 3
     */
    {
      id: 'ramesses-wall3',
      type: 'annotation',
      navPointId: "cb91d46a-a185-4e8d-bbfe-d40edb6c9332",
      file: 'https://static.mused.org/ramesses_wall3_v2_64.png',
      position: [-100, 28, -3],
      rotation: [-3.141, 1.418, -3.141],
      size: [79, 124]
    },

    /**
     * wall 4
     */
    {
      id: 'ptah-wall4',
      type: 'annotation',
      navPointId: "5955b2eb-53a5-49b2-bf39-d242b8b9e48c",
      file: 'https://static.mused.org/ptah_wall4_v5_64.png',
      position: [100, 38, 38],
      rotation: [0, 4.7, 0],
      size: [48, 128]
    },

    /**
     * wall 5
     */
    {
      id: 'ramesses-horus-anubis-wall5',
      type: 'annotation',
      navPointId: "5955b2eb-53a5-49b2-bf39-d242b8b9e48c",
      file: 'https://static.mused.org/ramesses_horus_anubis_wall5_v2_64.png',
      position: [11, 39, 100],
      rotation: [0, 3.34, 0],
      size: [125, 132]
    },

    {
      id: 'ra-solar-boat-wall5',
      type: 'annotation',
      navPointId: "e4011646-5f48-4cdb-ace6-6ecb6409d023",
      file: 'https://static.mused.org/ra_solar_boat_wall5_v3_64.png',
      position: [63, 48, 100],
      rotation: [0, 3.4, 0.05],
      size: [103, 46]
    },

    {
      id: 'hereret-wall5',
      type: 'annotation',
      navPointId: "f2fc8fda-73f9-491b-bb41-e619f851f225",
      file: 'https://static.mused.org/hereret_wall5_v2_64.png',
      position: [-6, -17, 100],
      rotation: [0, 3.14, 0],
      size: [47, 36]
    },
    {
      id: 'hours-wall5',
      type: 'annotation',
      navPointId: "f2fc8fda-73f9-491b-bb41-e619f851f225",
      file: 'https://static.mused.org/hours_wall5_v2_64.png',
      position: [-2.6, -9.5, 100],
      rotation: [0, 3.12, 0],
      size: [204, 63]
    },

    /**
     * wall 6
     */
    {
      id: 'khepri-wall6',
      type: 'annotation',
      navPointId: "fd05c823-2fb4-4bbc-b2b1-d232e9e4e889",
      file: 'https://static.mused.org/khepri_wall6_v2_64.png',
      position: [-100, 49.5, 24],
      rotation: [1.957, 1.005, -1.993],
      size: [110, 138]
    },
    {
      id: 'ra-khepri-wall6',
      type: 'annotation',
      navPointId: "fd05c823-2fb4-4bbc-b2b1-d232e9e4e889",
      file: 'https://static.mused.org/ra_khepri_v4_64.png',
      position: [-100, 14, -4],
      rotation: [0, 1.57, 0],
      size: [12, 32]
    },

  ]
};


//  "initialNavPoint": 7,



class Annotation {
  constructor({ id, navPointId, file, position, rotation, size }) {
    this.id = id;
    this.navPointId = navPointId;
    this.file = file;
    this.position = position;
    this.rotation = rotation;
    this.size = size;

    this.createOverlayFace();
  }

  createOverlayFace() {
    const { app, scene, camera, renderer } = Store.getState();
    const loader = new THREE.TextureLoader();
    const [faceW, faceH] = this.size;
    const position = this.position;
    const rotation = this.rotation;

    const vertexShader = `
      varying vec2 vUv;

      void main() {
        vUv = uv;
        gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
      }
    `;

    const fragmentShader = `
      uniform sampler2D map;
      uniform float overlayAlpha;
      varying vec2 vUv;

      void main() {
        vec4 texColor = texture2D(map, vUv);
        vec4 finalColor = vec4(0.0,0.0,0.0,0.0);
        if (texColor.a > 0.1) {
          texColor.rgb += 0.01 * texColor.a;
          // finalColor =  vec4(1.0, 1.0, 1.0, 0.2) * texColor;
          finalColor = vec4(texColor.rgb, 0.3);
          // finalColor = texColor;
        } else {
          finalColor = vec4(0.0,0.0,0.0,0.0);
        }
        gl_FragColor = finalColor; 
        // gl_FragColor = finalColor;
      }
    `; 

    loader.load(this.file, (overlayTex) => {
      // overlayTex.minFilter = THREE.LinearMipMapLinearFilter;
      // overlayTex.magFilter = THREE.LinearFilter;
      // overlayTex.anisotropy = 16;
      overlayTex.colorSpace = THREE.SRGBColorSpace;
      // overlayTex.needsUpdate = true;
      
      const overlayPlane = new THREE.PlaneGeometry(faceW, faceH);
      // this.overlayMaterial = new THREE.MeshPhongMaterial({
      //   map: overlayTex, 
      //   transparent: true,
      //   opacity: 0,
      //   depthWrite: true, 
      //   depthTest: false,
      // });
      // this.overlayMaterial = new THREE.ShaderMaterial({
      //   uniforms: {
      //     map: { value: overlayTex },
      //     overlayAlpha: { value: 0.0 },
      //   },
      //   vertexShader: vertexShader,
      //   fragmentShader: fragmentShader,
      //   transparent: true,
      //   depthWrite: true,
      //   depthTest: false,
      // });
      this.overlayMaterial = new THREE.MeshPhongMaterial({
        map: overlayTex,
        transparent: true,
        opacity: 0.0, // Set the alpha to 0.5
        depthWrite: true,
        depthTest: false,
        // color: 0xffffff, // Set color to white
        emissive: 0x2b271e, // Set emissive to 50% white to brighten the material
      });


      this.overlay = new THREE.Mesh(overlayPlane, this.overlayMaterial);
      this.overlay.rotation.set(rotation[0], rotation[1], rotation[2]);
      this.overlay.position.set(position[0], position[1], position[2]+0.01);
      this.overlay.renderOrder = 10;
      scene.add(this.overlay);

      // console.log("adding to bloom for overlay", this.overlay);
      // const store = Store.getState();

      // this.controls = new TransformControls(camera, renderer.domElement);
      // this.controls.attach(this.overlay);
      // this.controls.setMode( 'rotate' );
      // scene.add(this.controls);

      // // Add an event listener to log the rotation when the controls are used
      // this.controls.addEventListener('change', () => {
      //   console.log("Rotated to:", this.overlay.rotation);
      // });
    });

  }

  toggleFade() {
    const { app } = Store.getState();
    this.fadeIn(2000, () => {
      setTimeout(() => {
        this.fadeOut(2000, () => {
          setTimeout(() => {
            this.toggleFade();
          }, 500);
        });
      }, 500);
    });
  }

  fadeIn(duration = 600, callback) {
    const { app } = Store.getState();
    let progress = 0;

    // exception for sunrise
    if (this.id === "ra-khepri-wall6") {
      this.slideToPosition();
    }


    const interval = setInterval(() => {
      progress += 20;
      const factor = progress / duration;
      if (this.overlayMaterial) {
        this.overlayMaterial.opacity = 1 * factor;
        // this.overlayMaterial.uniforms.overlayAlpha.value = 1 * factor;
      }

      // app.envCube.dimScene(bgOpacity);
      // app.setLightIntensity(0.6);

      if (progress >= duration) {

        clearInterval(interval);
        if (callback) callback();
        // app.post.outline.selection.add(this.overlay);
        // app.post.bloom.selection.add(this.overlay);
      }
    }, 20);
  }

  fadeOut(duration = 600, callback) {
    const { app } = Store.getState();
    let progress = 0;
    const interval = setInterval(() => {
      progress += 20;
      const factor = 1 - (progress / duration);
      this.overlayMaterial.opacity = factor;
      // this.overlayMaterial.uniforms.overlayAlpha.value = factor;

      // const bgOpacity = 1 - (factor * 0.9);
      const bgOpacity = 1 - factor;

      // app.setLightIntensity(1);
      // app.envCube.dimScene(bgOpacity);

      if (progress >= duration) {
        clearInterval(interval);
        if (callback) callback();
      }
    }, 20);
  }

  slideToPosition(duration = 6000, callback) {
    const position = this.position;

    let progress = 0;
    const interval = setInterval(() => {
      progress += 20;
      const factor = 1 - (progress / duration);

      this.overlay.position.lerpVectors(new THREE.Vector3(position[0], position[1], position[2]+0.01), new THREE.Vector3(position[0], position[1]-20, position[2]+0.01), factor)

      if (progress >= duration) {
        clearInterval(interval);
        if (callback) callback();
      }
    }, 20);
  }

  switchMaterial(type) {
    if (!this.overlayMaterial.map) {
      console.warn('Texture not loaded yet.');
      return;
    }
    
    // const materialOptions = {
    //   map: this.overlayMaterial.map, 
    //   transparent: true,
    //   opacity: this.overlayMaterial.opacity,
    //   this.overlayMaterial.uniforms = factor;
    //   depthWrite: true, 
    //   depthTest: false,
    // };
    
    // if (type === "BASIC") {
    //   this.overlayMaterial = new THREE.MeshBasicMaterial(materialOptions);
    // } else { 
    //   this.overlayMaterial = new THREE.MeshPhongMaterial(materialOptions);
    // }
    
    // this.overlay.material = this.overlayMaterial;

  }
}

export default class AnnotationGraph {
  constructor() {
    const { currentNavPoint } = Store.getState();

    this.annotationLookup = {}; // This will store annotations by their id
    this.buildAnnotationGraph(annotationData);

    // setTimeout(() => {
    //   this.showAllForNavPointId(currentNavPoint.uuid);
    // }, 3000);
    // Create a new dat.GUI instance
    // Create a new dat.GUI instance
    // const gui = new dat.GUI();

    // // Define an object to hold the color, emissive, and opacity values
    // const materialController = {
    //   color: 0xf1d684,
    //   emissive: 0x404040,
    //   opacity: 1
    // };

    // // Add color controller to the GUI
    // gui.addColor(materialController, 'color').onChange((value) => {
    //   // Update the color of all annotation materials
    //   Object.values(this.annotationLookup).forEach(annotation => {
    //     if (annotation instanceof Annotation) {
    //       // annotation.overlay.material.color.setHex(value);
    //     }
    //   });
    // });

    // // Add emissive controller to the GUI
    // gui.addColor(materialController, 'emissive').onChange((value) => {
    //   // Update the emissive of all annotation materials
    //   Object.values(this.annotationLookup).forEach(annotation => {
    //     if (annotation instanceof Annotation) {
    //       annotation.overlay.material.emissive.setHex(value);
    //     }
    //   });
    // });

    // // Add opacity controller to the GUI
    // gui.add(materialController, 'opacity', 0, 1).onChange((value) => {
    //   // Update the opacity of all annotation materials
    //   Object.values(this.annotationLookup).forEach(annotation => {
    //     if (annotation instanceof Annotation) {
    //       annotation.overlay.material.opacity = value;
    //     }
    //   });
    // });
  }


  buildAnnotationGraph(node, parent = null) {
    const { scene } = Store.getState();

    if (node.type === 'group') {
      const group = new THREE.Group();
      group.position.set(...(node.position || [0, 0, 0]));
      node.children.forEach(childNode => this.buildAnnotationGraph(childNode, group));
      scene.add(group);
      this.annotationLookup[node.id] = group; 
    } else if (node.type === 'annotation') {
      const annotation = new Annotation(node);
      this.annotationLookup[node.id] = annotation; // Store the annotation by its id
    }
  }

  getAnnotationById(id) {
    return this.annotationLookup[id];
  }

  showAnnotationById(id) {
    const annotation = this.annotationLookup[id];
    if (annotation) {
      annotation.fadeIn();
    } else {
      console.warn(`Annotation with id ${id} not found.`);
    }
  }

  hideAnnotationById(id) {
    const annotation = this.annotationLookup[id];
    if (annotation) {
      annotation.fadeOut();
    } else {
      console.warn(`Annotation with id ${id} not found.`);
    }
  }

  // Hide all annotations
  hideAllAnnotations(duration=600) {
    Object.keys(this.annotationLookup).forEach(id => {
      const annotation = this.annotationLookup[id];
      if (annotation && typeof annotation.fadeOut === 'function') {
        annotation.fadeOut(duration);
      }
    });
  }

  showAllForNavPointId(navPointId) {
    // Iterate through all annotations
    Object.keys(this.annotationLookup).forEach(id => {
      const annotation = this.annotationLookup[id];
      
      // Check if the annotation has a navPointId and if it matches the given navPointId
      if (annotation && annotation.navPointId === navPointId) {
        // If it matches, fade the annotation in
        annotation.fadeIn();
      } else {
        if (typeof annotation.fadeOut === 'function') {
          // If it doesn't match, fade the annotation out
          annotation.fadeOut();
        }
      }
    });
  }
}
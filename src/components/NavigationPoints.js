import * as THREE from 'three';

import FloorMarker from './FloorMarker';

import Store from '../Store';
import DATA from '../../static/locales';



export class NavigationPoint {
  constructor({ uuid, position, floorPosition, rotation, image, isActive}) {
    // store
    const { debugMode } = Store.getState();
    this.uuid = uuid;
    this.position = position;
    this.floorPosition = floorPosition;
    this.rotation = rotation;
    this.image = image;
    this.isActive = isActive;

    // Create mesh
    const geometry = new THREE.SphereGeometry(isActive ? 0.3 : 0.2, 4, 4);
    const material = new THREE.MeshBasicMaterial({
      color: isActive ? 0x2a9df4 : 0xffffff, 
      wireframe: true
    });
    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.visible = debugMode;

    // Position
    this.mesh.position.set(position.x, position.y, position.z);
    
    // for raycast click event
    this.mesh.navigationPoint = this;

    // floormarker
    this.floormarker = new FloorMarker({ uuid, position, floorPosition, rotation, image })
  }

  toggleSphereVisibility() {
    const { debugMode } = Store.getState();
    if (this.mesh) {
      this.mesh.visible = debugMode;
    }
  }

  toggleFloorMarkerVisibility(visible) {
    if (this.floormarker && this.floormarker.group) { // Assuming floormarker has a mesh property
      this.floormarker.group.visible = visible;
    }
  }


  getMesh() {
    return this.mesh;
  }
}


export default class NavigationPoints {

  constructor() {
    // store
    const { scene } = Store.getState();

    // Create parent object
    const parent = new THREE.Group();

    // Add child objects
    this.navPoints = [];
    DATA[window.LANG].navPoints.forEach(navPoint => {
      const np = new NavigationPoint(navPoint);
      this.navPoints.push(np);

      // Add to parent group
      parent.add(np.getMesh());
    });

    // Position
    const offset = DATA[window.LANG].sceneSettings.navPoints.offsetPosition;
    parent.position.set(offset.x, offset.y, offset.z);
    
    // Rotation
    const rotOffset = DATA[window.LANG].sceneSettings.navPoints.offsetRotation;
    parent.rotation.set(
      THREE.MathUtils.degToRad(rotOffset.x),
      THREE.MathUtils.degToRad(rotOffset.y), 
      THREE.MathUtils.degToRad(rotOffset.z)
    );

    // Scale
    const scale = DATA[window.LANG].sceneSettings.navPoints.scale;
    parent.scale.set(scale, scale, scale);

    // add to scene
    scene.add(parent);
  }

  handleToggleDebugMode() {
    const { debugMode } = Store.getState();
    this.navPoints.forEach(np => {
      np.toggleSphereVisibility();
    }); 
  }

  handleToggleViewMode() {
    const { viewMode, floorMarkers } = Store.getState();

    floorMarkers.forEach(floorMarker => {

      if (viewMode === "ORBIT") {
        floorMarker.children.forEach(child => {
          if (child.geometry.type === 'RingGeometry') {
            child.material.opacity = 0.5;
          }
        });
      } else {
        floorMarker.children.forEach(child => {
          if (child.geometry.type === 'RingGeometry') {
            child.material.opacity = 0.1;
          }
        });
      }
    });
  }

  // Show all floor markers
  showFloorMarkers() {
    this.navPoints.forEach(np => {
      np.toggleFloorMarkerVisibility(true);
    });
  }

  // Hide all floor markers
  hideFloorMarkers() {
    this.navPoints.forEach(np => {
      np.toggleFloorMarkerVisibility(false);
    });
  }

  // Toggle the visibility of all floor markers
  toggleFloorMarkers() {
    const firstNavPoint = this.navPoints[0];
    const currentState = firstNavPoint && firstNavPoint.floormarker && firstNavPoint.floormarker.mesh ? firstNavPoint.floormarker.mesh.visible : false;
    this.navPoints.forEach(np => {
      np.toggleFloorMarkerVisibility(!currentState);
    });
  }

}


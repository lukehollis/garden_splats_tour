import * as THREE from 'three';


import Store from './Store';
import DATA from '../static/locales';
import { areVector3Equal } from './lib/util';


const calculatePinchDistance = (touch1, touch2) => {
  const dx = touch1.clientX - touch2.clientX;
  const dy = touch1.clientY - touch2.clientY;
  return Math.sqrt(dx * dx + dy * dy);
}


export default class PointerHandlers {
  constructor() {
    this.fadeTimeout = null;
    this.mouse = new THREE.Vector2();
    this.raycaster = new THREE.Raycaster();
    this.startPinchDistance = null;
  }

  handleMouseMove(event) {
    event.preventDefault();
    this.processInteractionMove(event.clientX, event.clientY, event.target);
  }

  handleTouchMove(event) {
    event.preventDefault();
    const { app, viewMode } = Store.getState();

    if (event.touches.length > 0) {
      if (viewMode === "FPV" && event.touches.length === 2) {
        const newPinchDistance = calculatePinchDistance(event.touches[0], event.touches[1]);

        if (this.startPinchDistance === null) {
          this.startPinchDistance = newPinchDistance;
        }

        const zoomSpeed = 0.1;  // Adjust speed as needed
        const direction = newPinchDistance > this.startPinchDistance ? 1 : -1;
        const zoomAmount = (newPinchDistance - this.startPinchDistance) * zoomSpeed;
        console.log(`startPinchDistance: ${this.startPinchDistance}, newPinchDistance: ${newPinchDistance}, direction: ${direction}, zoomAmount: ${zoomAmount}`);  // Debugging line


        app.cameraHandlers.handleZoom(zoomAmount);

        this.startPinchDistance = newPinchDistance;

      } else {
        this.startPinchDistance = null;

        const touch = event.touches[0];
        this.processInteractionMove(touch.clientX, touch.clientY, event.target);
      }
    }
  }

  processInteractionMove(clientX, clientY, eventTarget) {
    const store = Store.getState();
    const { app, camera, scene, floorMarkers } = store;
    event.preventDefault();

    const rect = eventTarget.getBoundingClientRect();
    this.mouse = new THREE.Vector2();
    this.mouse.x = ((clientX - rect.left) / rect.width) * 2 - 1;
    this.mouse.y = -((clientY - rect.top) / rect.height) * 2 + 1;

    this.raycaster.setFromCamera(this.mouse, camera);

    let intersects = [];
    if (app.dollhouse.gltf && app.dollhouse.gltf.scene) {
      intersects = this.raycaster.intersectObjects(app.dollhouse.gltf.scene.children, true);
    }

    for (let i = 0; i < intersects.length; i++) {

      if (intersects[i].face) {
        Store.setState({
          cursor: {
            position: intersects[i].point,
            rotation: new THREE.Quaternion().setFromUnitVectors(new THREE.Vector3(0, 1, 0), intersects[i].face.normal),
          },
        });
        break;
      }
    }

    let newHoveredFloorMarker = null;
    let floorMarkerIntersects = [];
    if (floorMarkers) {
      floorMarkerIntersects = this.raycaster.intersectObjects(floorMarkers, true);
    }

    for (let i = 0; i < floorMarkerIntersects.length; i++) {
      if (floorMarkerIntersects[i].object && floorMarkerIntersects[i].object.floorMarker) {
        newHoveredFloorMarker = floorMarkerIntersects[i].object.floorMarker; 
      }
    }
    
    if (newHoveredFloorMarker !== this.previousHoveredFloorMarker) {
      // Trigger onMouseLeave for the previously hovered floor marker
      if (this.previousHoveredFloorMarker) {
        this.previousHoveredFloorMarker.onMouseLeave();
      }

      // Update the previous hovered marker
      this.previousHoveredFloorMarker = newHoveredFloorMarker;

      // Trigger onMouseEnter for the new hovered floor marker
      if (newHoveredFloorMarker) {
        newHoveredFloorMarker.onMouseEnter();
      }
    }


    // use this when you need cursor ring 
    // this.handleCursorFade();
  }

  handleCursorFade() {
    // on movement, update the cursor opacity
    Store.setState({ cursorOpacity: 0.6 });
    // clear the existing fade timeout
    clearTimeout(this.fadeTimeout);

    // set a new fade timeout with updated time to fade the cursor on non-movement
    this.fadeTimeout = setTimeout(()  => {
      let progress = 0;
      let duration = 200;

      const interval = setInterval(() => {
        progress += 20;
        const factor = progress / duration;
        Store.setState({ cursorOpacity: 0.6-factor });
        if (progress >= duration) {
          clearInterval(interval);
          Store.setState({ cursorOpacity: 0.001 });
        }
      }, 20);
    }, 2000);
  }

  handleMouseClick(event) {
    this.processInteractionClick(event.clientX, event.clientY, event);
  }

  handleTouchTap(event) {
    if (event.touches.length > 0) {
      const touch = event.touches[0];
      this.processInteractionClick(touch.clientX, touch.clientY, event);
    }
  }

  processInteractionClick(clientX, clientY, event) {
    const { app, camera, scene, currentNavPoint, viewMode, debugMode, tourGuidedMode } = Store.getState();

    // Check if the click is on an HTML button or other UI elements you want to exclude
    if (event.target.tagName === 'BUTTON' || event.target.classList.contains('some-other-class')) {
      return;
    }
    
    event.preventDefault();

    // for now, prevent navigation out of orbit mode in guided tour mode 
    if (tourGuidedMode && viewMode === "ORBIT") return;

    // Calculate mouse position in normalized device coordinates
    this.mouse.x = (clientX / window.innerWidth) * 2 - 1;
    this.mouse.y = -(clientY / window.innerHeight) * 2 + 1;

    // Update the raycaster with the new mouse position
    this.raycaster.setFromCamera(this.mouse, camera);

    // first check if the user clicked on a specific nav point
    // Array to store all intersected objects
    const intersects = this.raycaster.intersectObjects(scene.children, true);

    // Loop through all intersected objects and find the mesh you're interested in
    for (let intersect of intersects) {
      if (intersect.object.floorMarker) {
        // don't try to navigate to point you're currently at
        if (viewMode === "ORBIT" || intersect.object.floorMarker.uuid !== currentNavPoint.uuid) {
          // navigate to the newly clicked nav point
          app.cameraHandlers.handleNavigation(intersect.object.floorMarker, { viewMode: "FPV", });
          return;
        }
      }

      if (debugMode && intersect.object.navigationPoint) {
        // don't try to navigate to point you're currently at
        if (viewMode === "ORBIT" || intersect.object.navigationPoint.uuid !== currentNavPoint.uuid) {
          // navigate to the newly clicked nav point
          app.cameraHandlers.handleNavigation(intersect.object.navigationPoint, {
            viewMode: "FPV",
          });
          return;
        }
      }
    }

    // next, if user didn't click on a specific nav point, navigate to the 
    // nearest nav point to their click vector
    // Get the click vector
    const clickDirection = this.raycaster.ray.direction.clone().normalize();
    const currentNavPointVector = new THREE.Vector3(currentNavPoint.floorPosition.x, currentNavPoint.floorPosition.y, currentNavPoint.floorPosition.z);

    let nearestNavPoint;
    let smallestAngle = Infinity;
    let nearestDistance = Infinity;

    for (const navPoint of DATA[window.LANG].navPoints) {  // Make sure DATA[window.LANG].navPoints is available in this context or get it from the Store
      if (viewMode === "FPV" && navPoint.uuid === currentNavPoint.uuid) continue;

      const navPointVector = new THREE.Vector3(navPoint.floorPosition.x, navPoint.floorPosition.y, navPoint.floorPosition.z);
      const directionToNavPoint = navPointVector.clone().sub(currentNavPointVector).normalize();

      const angle = clickDirection.angleTo(directionToNavPoint);
      const distance = currentNavPointVector.distanceTo(navPointVector);

      if (angle < Math.PI / 8 && angle < smallestAngle && distance < nearestDistance) {
        nearestNavPoint = navPoint;
        smallestAngle = angle;
        nearestDistance = distance;
      }
    }

    if (nearestNavPoint) {
      app.cameraHandlers.handleNavigation(nearestNavPoint, {
        viewMode: "FPV",
      });
    } 

  }
}

